<?php


$og_image_photo_src=CFile::GetPath($arResult['PROPERTIES']['og_image']['VALUE']);

use \Bitrix\Main\Page\Asset;

Asset::getInstance()->addString('<meta property="og:title"' . $arResult['PROPERTIES']['og_title']['VALUE'] . '">');
Asset::getInstance()->addString('<meta property="og:image"' . $og_image_photo_src . '">');

Asset::getInstance()->addJs("https://cdn.jsdelivr.net/npm/lazyload@2.0.0-rc.2/lazyload.js");
Asset::getInstance()->addJs("https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js");
Asset::getInstance()->addCss("https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css");
Asset::getInstance()->addJs("https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js");