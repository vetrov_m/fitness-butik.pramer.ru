<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-detail">
    <?if($arResult["NAME"]):?>
        <h2>заголовок элемента: <?=$arResult["NAME"]?></h2>
    <?endif;?>
    <?if($arResult["ACTIVE_FROM"]):?>
        <div>документ опубликован: <?=$arResult["ACTIVE_FROM"]?> -
            <?=intdiv((time()- MakeTimeStamp($arResult["ACTIVE_FROM"], CSite::GetDateFormat())), 60). " минут " .
            ((time()- MakeTimeStamp($arResult["ACTIVE_FROM"], CSite::GetDateFormat())) % 60). " секунд назад";?>
        </div>
    <?endif;?>
    <?if($arResult["SHOW_COUNTER"]):?>
        <div>количество просмотров:
            <span id="count_viev">
                <?php
                $frame = $this->createFrame()->begin('Количество просмотров: Загрузка...');
                ?>
                <?= $arResult['SHOW_COUNTER'] ? $arResult['SHOW_COUNTER'] : 0; ?>
                <?php $frame->end(); ?>
            </span>
        </div>
    <?endif;?>
    <?if($arResult["DETAIL_PICTURE"]['SRC']):?>
        <div>
            <?php
            // условный размер под блок верстки
            $detail_img = CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width'=>600 , 'height'=>900), BX_RESIZE_IMAGE_PROPORTIONAL , true);
            ?>
            <img class="lazyload" src="data:image/gif;base64,R0lGODlhWAIpAYAAAP///wAAACH5BAEAAAEALAAAAABYAikBAAL+jI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubq7vL2+v7CxwsPExcbHyMnKy8zNzs/AwdLT1NXW19jZ2tvc3d7f0NHi4+Tl5ufo6err7O3u7+Dh8vP09fb3+Pn6+/z9/v/w8woMCBBAsaPIgwocKFDBs6fAgxosSJFCtavIgxo8b+jRw7evwIMqTIkSRLmjyJMqXKlSxbunwJM6bMmTRr2ryJM6fOnTx7+vwJNKjQoUSLGj2KNKnSpUybOn0KNarUqVSrWr2KNavWrVy7ev0KNqzYsWTLmj2LNq3atWzbun0LN67cuXTr2r2LN6/evXz7+v0LOLDgwYQLGz6MOLHixYwbO34MObLkyZQrW76MObPmzZw7e/4MOrTo0aRLmz6NOrXq1axbu34NO7bs2bRr276NO7fu3bx7+/4NPLjw4cSLGz+OPLny5cybO38OPbr06dSrW7+OPbv27dy7e/8OPrz48eTLmz+PPr369ezbu38PP778+fTr27+PP7/+/fx2+/v/D2CAAg5IYIEGHohgggouyGCDDj4IYYQSTkhhhRZeiGGGGm7IYYcefghiiCKOSGKJJp6IYooqrshiiy6+CGOMMs5IY4023ohjjjruyGOPPv4IZJBCDklkkUYeiWSSSi7JZJNOPglllFJOSWWVVl6JpUoFAAA7" data-src="<?=$detail_img['src']?>"
            <?php if($arResult['DETAIL_PICTURE']['ALT']) echo "alt=\"".$arResult["DETAIL_PICTURE"]['ALT']."\"";?>
            <?php if($arResult['DETAIL_PICTURE']['TITLE']) echo "title=\"".$arResult["DETAIL_PICTURE"]['TITLE']."\"";?>
            width="<?=$detail_img['width']?>" height="<?=$detail_img['height']?>">
        </div>
    <?endif;?>
    <?if($arResult["DETAIL_TEXT"]):?>
        <div>вывод детального описания: <?=$arResult["DETAIL_TEXT"]?></div>
    <?endif;?>

    <?if($arResult["PROPERTIES"]['more_photos']['VALUE'][0] > 0):?>
        <?php
        foreach($arResult["PROPERTIES"]['more_photos']['VALUE'] as $key=>$value):?>

            <?php
            //размеры согласно требованиям
            $more_photo_src_prev = CFile::ResizeImageGet($value, array('width'=>200 , 'height'=>200), BX_RESIZE_IMAGE_PROPORTIONAL , true);

            $more_photo_src=CFile::GetPath($value);

            ?>
            <a data-fancybox="gallery" href="<?=$more_photo_src?>">
            <img class="lazyload" src="data:image/gif;base64,R0lGODlhyADIAIAAAP///wAAACH5BAEAAAEALAAAAADIAMgAAAL+jI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubq7vL2+v7CxwsPExcbHyMnKy8zNzs/AwdLT1NXW19jZ2tvc3d7f0NHi4+Tl5ufo6err7O3u7+Dh8vP09fb3+Pn6+/z9/v/w8woMCBBAsaPIgwocKFDBs6fAgxosSJFCtavIgxo8YHjRw7eixTAAA7" data-src="<?=$more_photo_src_prev['src']?>"
                 width="<?=$more_photo_src_prev['width']?>" height="<?=$more_photo_src_prev['height']?>">
            </a>

            <?php

            unset($more_photo_src, $more_photo_src_prev);

        endforeach; ?>

    <?endif;?>
</div>